package com.twuc.webApp.controller;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.client.AutoConfigureWebClient;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureWebClient
public class ErrorControllerTest {
    @Autowired
    private TestRestTemplate template;

    // test section 2.1
    @Test
    void returnError() {
        ResponseEntity<String> responseEntity = template
                .getForEntity("/api/errors/default", String.class);
        assertEquals(HttpStatus.NOT_FOUND, responseEntity.getStatusCode());
    }

    // test section 2.2
    @Test
    void should_handle_exception() {
        ResponseEntity<String> responseEntity = template
                .getForEntity("/api/errors/default", String.class);
        assertEquals(HttpStatus.NOT_FOUND, responseEntity.getStatusCode());
        assertEquals("handle RuntimeException", responseEntity.getBody());
    }

    // test section 2.3
    @Test
    void should_handle_with_one_exception_handle() {
        ResponseEntity<String> responseEntity = template
                .getForEntity("/api/errors/null-pointer", String.class);
        assertEquals(HttpStatus.I_AM_A_TEAPOT, responseEntity.getStatusCode());
        assertEquals("Something wrong with the argument", responseEntity.getBody());
    }

    // test section 2.4
    @Test
    void should_handle_exception_in_two_controller() {
        ResponseEntity<String> responseEntity = template
                .getForEntity("/api/sister-errors/illegal-argument", String.class);
        assertEquals(HttpStatus.I_AM_A_TEAPOT, responseEntity.getStatusCode());
        assertEquals("Something wrong with brother or sister.", responseEntity.getBody());
    }
}
