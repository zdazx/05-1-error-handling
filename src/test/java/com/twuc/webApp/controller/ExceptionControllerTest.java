package com.twuc.webApp.controller;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.client.AutoConfigureWebClient;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureWebClient
public class ExceptionControllerTest {

    @Autowired
    private TestRestTemplate template;

    // test section 2.6
    @Test
    void should_illegal_exception() {
        ResponseEntity<String> responseEntity = template
                .getForEntity("/api/brother-errors/illegal-argument", String.class);
        assertEquals(HttpStatus.I_AM_A_TEAPOT, responseEntity.getStatusCode());
        assertEquals("Something wrong with brother or sister.", responseEntity.getBody());
    }
}
