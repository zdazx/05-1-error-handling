package com.twuc.webApp.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ExceptionController {

    @GetMapping("/api/brother-errors/illegal-argument")
    public ResponseEntity<String> handleException(){
        throw new IllegalArgumentException();
    }
}
