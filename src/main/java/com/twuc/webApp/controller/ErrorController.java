package com.twuc.webApp.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.security.AccessControlException;

@RestController
@ControllerAdvice
public class ErrorController {
    @GetMapping("/api/errors/default")
    public ResponseEntity<String> getError(){
        throw new RuntimeException();
    }
    @GetMapping("/api/exceptions/access-control-exception")
    public ResponseEntity<String> getAccessControlException(){
        throw new AccessControlException("");
    }

    @ExceptionHandler
    public ResponseEntity<String> handle(RuntimeException exception){
        return ResponseEntity.status(404).body("handle " + exception.getClass().getSimpleName());
    }

    @ExceptionHandler
    public ResponseEntity<String> getErrorMessage(AccessControlException exception){
        return ResponseEntity.status(403).body("handle new " + exception.getClass().getSimpleName());
    }

    // test section 2.3
    @GetMapping("/api/errors/null-pointer")
    public ResponseEntity<String> handleNullPointerException(){
        throw new NullPointerException();
    }

    @GetMapping("/api/errors/arithmetic")
    public ResponseEntity<String> handleArithmeticException(){
        throw new ArithmeticException();
    }

    @ExceptionHandler({NullPointerException.class, ArithmeticException.class})
    public ResponseEntity<String> handleWithOneResult(Exception exception){
        return ResponseEntity.status(418).body("Something wrong with the argument");
    }

    // test section 2.4
    @GetMapping("/api/sister-errors/illegal-argument")
    public ResponseEntity<String> handleExceptionInTwoDifference(){
        throw new IllegalArgumentException();
    }

    @ExceptionHandler
    public ResponseEntity<String> handleTheSameExceptionInDifferenceController(IllegalArgumentException exception){
        return ResponseEntity.status(418).body("Something wrong with brother or sister.");
    }

}
